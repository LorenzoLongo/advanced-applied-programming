﻿using Globals.Interfaces;
using LogicLayer.Maze;
using LogicLayer.Maze_Solver;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Threading;
using System.Windows.Forms;
using System.Windows.Input;

namespace MazeGamePlayGeneratorSolver
{
    public partial class MazeForm : Form
    {
        private const int _width = 30, _height = 20, _cellsize = 20, _interval = 10;
        private int startX, startY, endX, endY, seconds;
        private string ballCollision = "None";

        private System.Drawing.Size cellSz = new Size(_cellsize, _cellsize);
        private List<Bitmap> mazeProgress, pathProgress;

        private IImageBase drawBall, drawWind, drawEndPoint;
        private IGamePhysics physics;

        private IMazeGeneratorBase primGenerator;
        private IMazeSolver breadthFirst;



        // Contructor => Sets three layer model and necessary starting components
        public MazeForm(IImageBase drawBall, IImageBase drawEndPoint, IImageBase drawWind, IGamePhysics physics, IMazeGeneratorBase primGenerator, IMazeSolver breadthFirst)
        {
            this.drawBall = drawBall;
            this.drawEndPoint = drawEndPoint;
            this.drawWind = drawWind;

            this.physics = physics;

            this.primGenerator = primGenerator;
            this.breadthFirst = breadthFirst;

            InitializeComponent();
            powerBar.Maximum = 10;
            powerBar.Value = 0;

            drawBall.Left = drawBall.Top = drawEndPoint.Left = drawEndPoint.Top = drawWind.Left = drawWind.Top = 1000;
            startXComboBox.SelectedIndex = startYComboBox.SelectedIndex = endXComboBox.SelectedIndex = endYComboBox.SelectedIndex = 0;

            // Eliminate flicker that is caused by progressive redrawing
            this.DoubleBuffered = true;
        }



        // Searches if the ball is to the left, to the right, above or under the wind control
        private void DetectBallCollision()
        {
            if (drawBall.Left.Equals(drawWind.Left) && drawBall.Top.Equals(drawWind.Top + 20)) lblBallCollision.Text = ballCollision = "Top";
            else if (drawBall.Left.Equals(drawWind.Left) && drawBall.Top.Equals(drawWind.Top - 20)) lblBallCollision.Text = ballCollision = "Bottom";
            else if (drawBall.Left.Equals(drawWind.Left + 20) && drawBall.Top.Equals(drawWind.Top)) lblBallCollision.Text = ballCollision = "Left";
            else if (drawBall.Left.Equals(drawWind.Left - 20) && drawBall.Top.Equals(drawWind.Top)) lblBallCollision.Text = ballCollision = "Right";
            else lblBallCollision.Text = "No Collision Detected";
        }

        // Method to calculate the physics of the ball and redraw it
        private void UpdateBall(string ballCollisionSide)
        {
            if (ballCollisionSide.Equals("Left"))
            {
                drawBall.Update(drawBall.Left + 20, drawBall.Top);
                ballCollision = "None";
            }
            else if (ballCollisionSide.Equals("None")) ;
        }



        // Method which starts the threads to generate the maze
        // https://github.com/garyng/Maze
        private void DrawMaze(IMazeGeneratorrBase maze)
        {
            mazeProgress = new List<Bitmap>();
            maze.MazeProcess += (int total, int finsihed) =>
            {
                mazeProgress.Add(maze.DrawMaze(startX, startY, cellSz));
            };
            new Thread(delegate ()
            {
                using (maze)
                {
                    maze.GenerateMaze(startX, startY);
                }
            })
            { IsBackground = true }.Start();

            maze.MazeCompleted += () =>
            {
                new Thread(delegate ()
                {
                    for (int i = 0; i < mazeProgress.Count; i++)
                    {
                        this.Invoke((MethodInvoker)delegate ()
                        {
                            this.BackgroundImage = mazeProgress[i];
                        });
                        Thread.Sleep(_interval);
                    }
                    primGenarator = maze;
                })
                { IsBackground = true }.Start();
            };
            btnSolveMaze.Enabled = btnStartGame.Enabled = btnStopGame.Enabled = true;
        }

        // Method to draw the ball, endpoint and wind control onto the maze
        protected override void OnPaint(PaintEventArgs e)
        {
            Graphics draw = e.Graphics;

            drawEndPoint.DrawImage(draw);
            drawBall.DrawImage(draw);
            drawWind.DrawImage(draw);

            base.OnPaint(e);
        }



        // Method to start generating a prim algorithm based maze
        private void btnGenerateMaze_Click(object sender, EventArgs e)
        {
            btnSolveMaze.Enabled = btnStartGame.Enabled = btnStopGame.Enabled = false;
            DrawMaze(new Prim(_width, _height));

            drawWind.Left = drawWind.Top = 1;

            drawBall.Left = (startX * _cellsize) + 1;
            drawBall.Top = (startY * _cellsize) + 1;

            drawEndPoint.Left = (endX * _cellsize) + 1;
            drawEndPoint.Top = (endY * _cellsize) + 1;
        }

        // Method to solve the previous generated maze with breadth first algorithm
        private void btnSolveMaze_Click(object sender, EventArgs e)
        {
            pathProgress = new List<Bitmap>();
            pathProgress.Clear();
            BreadthFirst breadthFirst = new BreadthFirst(primGenarator);
            foreach (var node in breadthFirst.SolveMaze(startX, startY, endX, endY, _width, _height))
            {
                pathProgress.Add(breadthFirst.DrawShortestPath(node.Coordinates.X, node.Coordinates.Y, _cellsize));
            }

            this.BackgroundImage = pathProgress[pathProgress.Count - 1];
            this.Refresh();
        }

        // Method to start playing the maze game 
        private void btnStartGame_Click(object sender, EventArgs e)
        {
            timerGameLoop.Start();
            timerControls.Start();
        }

        // Method to stop playing the maze game and do a reset
        private void btnStopGame_Click(object sender, EventArgs e)
        {
            drawBall.Left = (startX * _cellsize) + 1;
            drawBall.Top = (startY * _cellsize) + 1;

            drawWind.Left = drawWind.Top = 1;

            timerGameLoop.Stop();
            timerControls.Stop();
        }



        // Set controls and results when a certain key is pressed
        private void DetectInput()
        {
            if (Keyboard.IsKeyDown(Key.D))
                if (drawWind.Left + 20 < 600)
                {
                    drawWind.Left = drawWind.Left + 20;
                    drawWind.Top = drawWind.Top;
                }
            if (Keyboard.IsKeyDown(Key.Q))
                if (drawWind.Left - 20 > 0)
                {
                    drawWind.Left = drawWind.Left - 20;
                    drawWind.Top = drawWind.Top;
                }
            if (Keyboard.IsKeyDown(Key.S))
                if (drawWind.Top + 20 < 400)
                {
                    drawWind.Left = drawWind.Left;
                    drawWind.Top = drawWind.Top + 20;
                }
            if (Keyboard.IsKeyDown(Key.Z))
                if (drawWind.Top - 20 > 0)
                {
                    drawWind.Left = drawWind.Left;
                    drawWind.Top = drawWind.Top - 20;
                }
            if (Keyboard.IsKeyDown(Key.B))
            {
                powerCounter.Start();
            }
            if (Keyboard.IsKeyUp(Key.B))
            {
                //DetectBallCollision();
                powerCounter.Stop();
                lblSeconds.Text = "0 seconds counted";
                powerBar.Value = seconds = 0;
            }
        }



        // Gameloop of the maze game
        private void timerGameLoop_Tick(object sender, EventArgs e)
        {
            UpdateBall(ballCollision);
            this.Refresh();
        }

        // Loop to use the game controls
        private void timerControls_Tick(object sender, EventArgs e)
        {
            DetectInput();
        }

        // Loop to set the windpower (500ms to add +1 power)
        private void powerCounter_Tick(object sender, EventArgs e)
        {
            seconds++;
            lblSeconds.Text = seconds + " seconds counted";
            if (powerBar.Value < 10) powerBar.Value += 1;
        }



        // Method which checks if the start and end point coordinates are equal
        private bool CheckSelectedStartEndPoints()
        {
            if (!startX.Equals(endX) || !startY.Equals(endY)) return true;
            return false;
        }

        // Updates X coordinate of the endpoint and checks if end and start coordinates are equal. If they are, the "Generate" button is disabled.
        private void startXComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            startX = int.Parse(startXComboBox.SelectedItem.ToString());

            if (CheckSelectedStartEndPoints() == true) btnGenerateMaze.Enabled = true;
            else btnGenerateMaze.Enabled = false;

            lblStartPointCoordinates.Text = "Start Point: {X=" + startX + ",Y=" + startY + "}";
        }

        // Updates Y coordinate of the start point and checks if end and start coordinates are equal. If they are, the "Generate" button is disabled.
        private void startYComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            startY = int.Parse(startYComboBox.SelectedItem.ToString());

            if (CheckSelectedStartEndPoints() == true) btnGenerateMaze.Enabled = true;
            else btnGenerateMaze.Enabled = false;

            lblStartPointCoordinates.Text = "Start Point: {X=" + startX + ",Y=" + startY + "}";
        }

        // Updates X coordinate of the end point and checks if end and start coordinates are equal. If they are, the "Generate" button is disabled.
        private void EndXComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            endX = int.Parse(endXComboBox.SelectedItem.ToString());

            if (CheckSelectedStartEndPoints() == true) btnGenerateMaze.Enabled = true;
            else btnGenerateMaze.Enabled = false;

            lblEndPointCoordinates.Text = "End Point: {X=" + endX + ",Y=" + endY + "}";
        }

        private IMazeGeneratorrBase primGenarator;
        // Updates Y coordinate of the end point and checks if end and start coordinates are equal. If they are, the "Generate" button is disabled.
        private void endYComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            endY = int.Parse(endYComboBox.SelectedItem.ToString());

            if (CheckSelectedStartEndPoints() == true) btnGenerateMaze.Enabled = true;
            else btnGenerateMaze.Enabled = false;

            lblEndPointCoordinates.Text = "End Point: {X=" + endX + ",Y=" + endY + "}";
        }
    }
}
