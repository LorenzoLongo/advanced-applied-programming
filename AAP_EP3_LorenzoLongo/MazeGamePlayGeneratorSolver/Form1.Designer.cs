﻿namespace MazeGamePlayGeneratorSolver
{
    partial class MazeForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MazeForm));
            this.timerGameLoop = new System.Windows.Forms.Timer(this.components);
            this.btnStartGame = new System.Windows.Forms.Button();
            this.btnStopGame = new System.Windows.Forms.Button();
            this.lblEndPointX = new System.Windows.Forms.Label();
            this.startYComboBox = new System.Windows.Forms.ComboBox();
            this.endYComboBox = new System.Windows.Forms.ComboBox();
            this.endXComboBox = new System.Windows.Forms.ComboBox();
            this.startXComboBox = new System.Windows.Forms.ComboBox();
            this.lblStartPointY = new System.Windows.Forms.Label();
            this.lbEndPointY = new System.Windows.Forms.Label();
            this.lblStartPointX = new System.Windows.Forms.Label();
            this.btnGenerateMaze = new System.Windows.Forms.Button();
            this.lblGameInfo = new System.Windows.Forms.Label();
            this.lblWarning = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lblInfoGame = new System.Windows.Forms.Label();
            this.lblInfoText = new System.Windows.Forms.Label();
            this.lblWindPower = new System.Windows.Forms.Label();
            this.btnSolveMaze = new System.Windows.Forms.Button();
            this.powerBar = new System.Windows.Forms.ProgressBar();
            this.lblSeconds = new System.Windows.Forms.Label();
            this.lblBallCollision = new System.Windows.Forms.Label();
            this.lblEndPointCoordinates = new System.Windows.Forms.Label();
            this.lblStartPointCoordinates = new System.Windows.Forms.Label();
            this.timerControls = new System.Windows.Forms.Timer(this.components);
            this.powerCounter = new System.Windows.Forms.Timer(this.components);
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // timerGameLoop
            // 
            this.timerGameLoop.Interval = 10;
            this.timerGameLoop.Tick += new System.EventHandler(this.timerGameLoop_Tick);
            // 
            // btnStartGame
            // 
            this.btnStartGame.Enabled = false;
            this.btnStartGame.Font = new System.Drawing.Font("Gill Sans MT", 8.25F);
            this.btnStartGame.Location = new System.Drawing.Point(176, 162);
            this.btnStartGame.Name = "btnStartGame";
            this.btnStartGame.Size = new System.Drawing.Size(127, 23);
            this.btnStartGame.TabIndex = 0;
            this.btnStartGame.Text = "Start Game";
            this.btnStartGame.UseVisualStyleBackColor = true;
            this.btnStartGame.Click += new System.EventHandler(this.btnStartGame_Click);
            // 
            // btnStopGame
            // 
            this.btnStopGame.Enabled = false;
            this.btnStopGame.Font = new System.Drawing.Font("Gill Sans MT", 8.25F);
            this.btnStopGame.Location = new System.Drawing.Point(176, 191);
            this.btnStopGame.Name = "btnStopGame";
            this.btnStopGame.Size = new System.Drawing.Size(127, 23);
            this.btnStopGame.TabIndex = 1;
            this.btnStopGame.Text = "Stop Game";
            this.btnStopGame.UseVisualStyleBackColor = true;
            this.btnStopGame.Click += new System.EventHandler(this.btnStopGame_Click);
            // 
            // lblEndPointX
            // 
            this.lblEndPointX.AutoSize = true;
            this.lblEndPointX.Font = new System.Drawing.Font("Gill Sans MT", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEndPointX.Location = new System.Drawing.Point(22, 106);
            this.lblEndPointX.Name = "lblEndPointX";
            this.lblEndPointX.Size = new System.Drawing.Size(39, 16);
            this.lblEndPointX.TabIndex = 5;
            this.lblEndPointX.Text = "End X:";
            // 
            // startYComboBox
            // 
            this.startYComboBox.FormattingEnabled = true;
            this.startYComboBox.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10",
            "11",
            "12",
            "13",
            "14",
            "15",
            "16",
            "17",
            "18"});
            this.startYComboBox.Location = new System.Drawing.Point(99, 83);
            this.startYComboBox.Name = "startYComboBox";
            this.startYComboBox.Size = new System.Drawing.Size(53, 21);
            this.startYComboBox.TabIndex = 6;
            this.startYComboBox.SelectedIndexChanged += new System.EventHandler(this.startYComboBox_SelectedIndexChanged);
            // 
            // endYComboBox
            // 
            this.endYComboBox.FormattingEnabled = true;
            this.endYComboBox.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10",
            "11",
            "12",
            "13",
            "14",
            "15",
            "16",
            "17",
            "18"});
            this.endYComboBox.Location = new System.Drawing.Point(99, 123);
            this.endYComboBox.Name = "endYComboBox";
            this.endYComboBox.Size = new System.Drawing.Size(53, 21);
            this.endYComboBox.TabIndex = 7;
            this.endYComboBox.SelectedIndexChanged += new System.EventHandler(this.endYComboBox_SelectedIndexChanged);
            // 
            // endXComboBox
            // 
            this.endXComboBox.FormattingEnabled = true;
            this.endXComboBox.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10",
            "11",
            "12",
            "13",
            "14",
            "15",
            "16",
            "17",
            "18",
            "19",
            "20",
            "21",
            "22",
            "23",
            "24",
            "25",
            "26",
            "27",
            "28"});
            this.endXComboBox.Location = new System.Drawing.Point(25, 122);
            this.endXComboBox.Name = "endXComboBox";
            this.endXComboBox.Size = new System.Drawing.Size(53, 21);
            this.endXComboBox.TabIndex = 8;
            this.endXComboBox.SelectedIndexChanged += new System.EventHandler(this.EndXComboBox_SelectedIndexChanged);
            // 
            // startXComboBox
            // 
            this.startXComboBox.FormattingEnabled = true;
            this.startXComboBox.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10",
            "11",
            "12",
            "13",
            "14",
            "15",
            "16",
            "17",
            "18",
            "19",
            "20",
            "21",
            "22",
            "23",
            "24",
            "25",
            "26",
            "27",
            "28"});
            this.startXComboBox.Location = new System.Drawing.Point(25, 83);
            this.startXComboBox.Name = "startXComboBox";
            this.startXComboBox.Size = new System.Drawing.Size(53, 21);
            this.startXComboBox.TabIndex = 9;
            this.startXComboBox.SelectedIndexChanged += new System.EventHandler(this.startXComboBox_SelectedIndexChanged);
            // 
            // lblStartPointY
            // 
            this.lblStartPointY.AutoSize = true;
            this.lblStartPointY.Font = new System.Drawing.Font("Gill Sans MT", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStartPointY.Location = new System.Drawing.Point(96, 67);
            this.lblStartPointY.Name = "lblStartPointY";
            this.lblStartPointY.Size = new System.Drawing.Size(42, 16);
            this.lblStartPointY.TabIndex = 10;
            this.lblStartPointY.Text = "Start Y:";
            // 
            // lbEndPointY
            // 
            this.lbEndPointY.AutoSize = true;
            this.lbEndPointY.Font = new System.Drawing.Font("Gill Sans MT", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbEndPointY.Location = new System.Drawing.Point(96, 107);
            this.lbEndPointY.Name = "lbEndPointY";
            this.lbEndPointY.Size = new System.Drawing.Size(38, 16);
            this.lbEndPointY.TabIndex = 11;
            this.lbEndPointY.Text = "End Y:";
            // 
            // lblStartPointX
            // 
            this.lblStartPointX.AutoSize = true;
            this.lblStartPointX.Font = new System.Drawing.Font("Gill Sans MT", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStartPointX.Location = new System.Drawing.Point(22, 67);
            this.lblStartPointX.Name = "lblStartPointX";
            this.lblStartPointX.Size = new System.Drawing.Size(43, 16);
            this.lblStartPointX.TabIndex = 12;
            this.lblStartPointX.Text = "Start X:";
            // 
            // btnGenerateMaze
            // 
            this.btnGenerateMaze.Enabled = false;
            this.btnGenerateMaze.Font = new System.Drawing.Font("Gill Sans MT", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGenerateMaze.Location = new System.Drawing.Point(25, 162);
            this.btnGenerateMaze.Name = "btnGenerateMaze";
            this.btnGenerateMaze.Size = new System.Drawing.Size(127, 23);
            this.btnGenerateMaze.TabIndex = 13;
            this.btnGenerateMaze.Text = "Generate Maze";
            this.btnGenerateMaze.UseVisualStyleBackColor = true;
            this.btnGenerateMaze.Click += new System.EventHandler(this.btnGenerateMaze_Click);
            // 
            // lblGameInfo
            // 
            this.lblGameInfo.AutoSize = true;
            this.lblGameInfo.Font = new System.Drawing.Font("Gill Sans MT", 9F);
            this.lblGameInfo.Location = new System.Drawing.Point(6, 20);
            this.lblGameInfo.Name = "lblGameInfo";
            this.lblGameInfo.Size = new System.Drawing.Size(219, 18);
            this.lblGameInfo.TabIndex = 14;
            this.lblGameInfo.Text = "Select the start and end point of the maze. ";
            // 
            // lblWarning
            // 
            this.lblWarning.AutoSize = true;
            this.lblWarning.Font = new System.Drawing.Font("Gill Sans MT", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblWarning.ForeColor = System.Drawing.Color.Red;
            this.lblWarning.Location = new System.Drawing.Point(6, 42);
            this.lblWarning.Name = "lblWarning";
            this.lblWarning.Size = new System.Drawing.Size(332, 16);
            this.lblWarning.TabIndex = 15;
            this.lblWarning.Text = "WARNING: The selected start and end point must be different! ";
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.White;
            this.groupBox1.Controls.Add(this.lblInfoGame);
            this.groupBox1.Controls.Add(this.lblInfoText);
            this.groupBox1.Controls.Add(this.lblWindPower);
            this.groupBox1.Controls.Add(this.btnSolveMaze);
            this.groupBox1.Controls.Add(this.powerBar);
            this.groupBox1.Controls.Add(this.lblSeconds);
            this.groupBox1.Controls.Add(this.lblBallCollision);
            this.groupBox1.Controls.Add(this.lblEndPointCoordinates);
            this.groupBox1.Controls.Add(this.btnStopGame);
            this.groupBox1.Controls.Add(this.lblStartPointCoordinates);
            this.groupBox1.Controls.Add(this.btnStartGame);
            this.groupBox1.Controls.Add(this.startYComboBox);
            this.groupBox1.Controls.Add(this.lblWarning);
            this.groupBox1.Controls.Add(this.lblEndPointX);
            this.groupBox1.Controls.Add(this.lblGameInfo);
            this.groupBox1.Controls.Add(this.endYComboBox);
            this.groupBox1.Controls.Add(this.btnGenerateMaze);
            this.groupBox1.Controls.Add(this.endXComboBox);
            this.groupBox1.Controls.Add(this.lblStartPointX);
            this.groupBox1.Controls.Add(this.startXComboBox);
            this.groupBox1.Controls.Add(this.lbEndPointY);
            this.groupBox1.Controls.Add(this.lblStartPointY);
            this.groupBox1.Location = new System.Drawing.Point(602, -7);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(384, 475);
            this.groupBox1.TabIndex = 16;
            this.groupBox1.TabStop = false;
            // 
            // lblInfoGame
            // 
            this.lblInfoGame.AutoSize = true;
            this.lblInfoGame.BackColor = System.Drawing.Color.Yellow;
            this.lblInfoGame.Font = new System.Drawing.Font("Gill Sans MT", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))));
            this.lblInfoGame.Location = new System.Drawing.Point(22, 288);
            this.lblInfoGame.Name = "lblInfoGame";
            this.lblInfoGame.Size = new System.Drawing.Size(65, 16);
            this.lblInfoGame.TabIndex = 23;
            this.lblInfoGame.Text = "Game info:";
            // 
            // lblInfoText
            // 
            this.lblInfoText.AutoSize = true;
            this.lblInfoText.BackColor = System.Drawing.Color.Yellow;
            this.lblInfoText.Font = new System.Drawing.Font("Gill Sans MT", 8.25F);
            this.lblInfoText.ForeColor = System.Drawing.Color.Crimson;
            this.lblInfoText.Location = new System.Drawing.Point(22, 304);
            this.lblInfoText.Name = "lblInfoText";
            this.lblInfoText.Size = new System.Drawing.Size(300, 96);
            this.lblInfoText.TabIndex = 24;
            this.lblInfoText.Text = resources.GetString("lblInfoText.Text");
            // 
            // lblWindPower
            // 
            this.lblWindPower.AutoSize = true;
            this.lblWindPower.Font = new System.Drawing.Font("Gill Sans MT", 8.25F);
            this.lblWindPower.Location = new System.Drawing.Point(22, 228);
            this.lblWindPower.Name = "lblWindPower";
            this.lblWindPower.Size = new System.Drawing.Size(67, 16);
            this.lblWindPower.TabIndex = 22;
            this.lblWindPower.Text = "Wind Power:";
            // 
            // btnSolveMaze
            // 
            this.btnSolveMaze.Enabled = false;
            this.btnSolveMaze.Font = new System.Drawing.Font("Gill Sans MT", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSolveMaze.Location = new System.Drawing.Point(25, 191);
            this.btnSolveMaze.Name = "btnSolveMaze";
            this.btnSolveMaze.Size = new System.Drawing.Size(127, 23);
            this.btnSolveMaze.TabIndex = 21;
            this.btnSolveMaze.Text = "Solve Maze";
            this.btnSolveMaze.UseVisualStyleBackColor = true;
            this.btnSolveMaze.Click += new System.EventHandler(this.btnSolveMaze_Click);
            // 
            // powerBar
            // 
            this.powerBar.BackColor = System.Drawing.Color.Green;
            this.powerBar.ForeColor = System.Drawing.Color.LawnGreen;
            this.powerBar.Location = new System.Drawing.Point(25, 247);
            this.powerBar.Name = "powerBar";
            this.powerBar.Size = new System.Drawing.Size(278, 23);
            this.powerBar.TabIndex = 20;
            // 
            // lblSeconds
            // 
            this.lblSeconds.AutoSize = true;
            this.lblSeconds.Location = new System.Drawing.Point(240, 228);
            this.lblSeconds.Name = "lblSeconds";
            this.lblSeconds.Size = new System.Drawing.Size(98, 13);
            this.lblSeconds.TabIndex = 19;
            this.lblSeconds.Text = "0 seconds counted";
            // 
            // lblBallCollision
            // 
            this.lblBallCollision.AutoSize = true;
            this.lblBallCollision.Location = new System.Drawing.Point(125, 228);
            this.lblBallCollision.Name = "lblBallCollision";
            this.lblBallCollision.Size = new System.Drawing.Size(109, 13);
            this.lblBallCollision.TabIndex = 18;
            this.lblBallCollision.Text = "No Collision Detected";
            // 
            // lblEndPointCoordinates
            // 
            this.lblEndPointCoordinates.AutoSize = true;
            this.lblEndPointCoordinates.Font = new System.Drawing.Font("Gill Sans MT", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEndPointCoordinates.Location = new System.Drawing.Point(173, 125);
            this.lblEndPointCoordinates.Name = "lblEndPointCoordinates";
            this.lblEndPointCoordinates.Size = new System.Drawing.Size(107, 16);
            this.lblEndPointCoordinates.TabIndex = 17;
            this.lblEndPointCoordinates.Text = "End Point: {X=0,Y=0}";
            // 
            // lblStartPointCoordinates
            // 
            this.lblStartPointCoordinates.AutoSize = true;
            this.lblStartPointCoordinates.Font = new System.Drawing.Font("Gill Sans MT", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStartPointCoordinates.Location = new System.Drawing.Point(173, 85);
            this.lblStartPointCoordinates.Name = "lblStartPointCoordinates";
            this.lblStartPointCoordinates.Size = new System.Drawing.Size(111, 16);
            this.lblStartPointCoordinates.TabIndex = 16;
            this.lblStartPointCoordinates.Text = "Start Point: {X=0,Y=0}";
            // 
            // timerControls
            // 
            this.timerControls.Tick += new System.EventHandler(this.timerControls_Tick);
            // 
            // powerCounter
            // 
            this.powerCounter.Interval = 250;
            this.powerCounter.Tick += new System.EventHandler(this.powerCounter_Tick);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.Location = new System.Drawing.Point(-2, -7);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(604, 411);
            this.pictureBox1.TabIndex = 17;
            this.pictureBox1.TabStop = false;
            // 
            // MazeForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Azure;
            this.ClientSize = new System.Drawing.Size(984, 402);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.groupBox1);
            this.MaximumSize = new System.Drawing.Size(1000, 441);
            this.MinimumSize = new System.Drawing.Size(1000, 441);
            this.Name = "MazeForm";
            this.Text = "Maze Generator/Solver Game";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Timer timerGameLoop;
        private System.Windows.Forms.Button btnStartGame;
        private System.Windows.Forms.Button btnStopGame;
        private System.Windows.Forms.Label lblEndPointX;
        private System.Windows.Forms.ComboBox startYComboBox;
        private System.Windows.Forms.ComboBox endYComboBox;
        private System.Windows.Forms.ComboBox endXComboBox;
        private System.Windows.Forms.ComboBox startXComboBox;
        private System.Windows.Forms.Label lblStartPointY;
        private System.Windows.Forms.Label lbEndPointY;
        private System.Windows.Forms.Label lblStartPointX;
        private System.Windows.Forms.Button btnGenerateMaze;
        private System.Windows.Forms.Label lblGameInfo;
        private System.Windows.Forms.Label lblWarning;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label lblEndPointCoordinates;
        private System.Windows.Forms.Label lblStartPointCoordinates;
        private System.Windows.Forms.Timer timerControls;
        private System.Windows.Forms.Label lblBallCollision;
        private System.Windows.Forms.Timer powerCounter;
        private System.Windows.Forms.Label lblSeconds;
        private System.Windows.Forms.ProgressBar powerBar;
        private System.Windows.Forms.Button btnSolveMaze;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label lblWindPower;
        private System.Windows.Forms.Label lblInfoGame;
        private System.Windows.Forms.Label lblInfoText;
    }
}

