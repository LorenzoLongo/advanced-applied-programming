﻿using Globals.Interfaces;
using LogicLayer.Draw;
using LogicLayer.Game_Physics;
using LogicLayer.Maze_Generator;
using LogicLayer.Maze_Solver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MazeGamePlayGeneratorSolver
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            IImageBase drawBall = new Ball();
            IImageBase drawEndPoint= new EndPoint();
            IImageBase drawWind = new Wind();

            IGamePhysics physics = new Physics();

            IMazeGeneratorBase primGenerator = new Prim(30, 20);
            IMazeSolver BFSolver = new BreadthFirst();

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MazeForm(drawBall, drawEndPoint, drawWind, physics, primGenerator, BFSolver));
        }
    }
}
