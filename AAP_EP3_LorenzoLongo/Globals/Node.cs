﻿using System.Collections.Generic;
using System.Drawing;

namespace Globals.Maze_Generator
{
    // Class to set the nodes of the walls of the maze
    // https://github.com/garyng/Maze
    public class Node
    {
        ///  ____________________
        /// |Left|Down|Right| Up |
        /// |_8__|_4__|__2__|_0__|
        private int wallIndex = 0;
        private const int _count = 4;

        private bool startPoint, isFrontier;

        private Node upper, lower, left, right;

        private List<NodeInfo> nodeInfo = new List<NodeInfo>();

        private Point coordinates;


        public int Count { get { return _count; } }
        public int MazeWall { get { return wallIndex; } set { wallIndex = value; } }

        public bool StartPoint { get { return startPoint; } set { startPoint = value; } }
        public bool Frontier { get { return isFrontier; } set { isFrontier = value; } }


        public Node Upper { get { return upper; } set { upper = value; } }
        public Node Lower { get { return lower; } set { lower = value; } }
        public Node Left { get { return left; } set { left = value; } }
        public Node Right { get { return right; } set { right = value; } }

        public Node this[int index] { get { return SetNode(index); } set { SetNode(index, value); } }


        public List<NodeInfo> Info { get { return nodeInfo; } set { nodeInfo = value; } }

        public Point Coordinates { get { return coordinates; } set { coordinates = value; } }



        // Get wall (destroyed yes or no)
        public bool GetWall(int index)
        {
            return (wallIndex & (1 << index)) == (1 << index);
        }

        // Destroy the wall (int wall = 1)
        public void DestroyWall(int index)
        {
            wallIndex |= 1 << index;
        }


        // Sets the node type (node up, down, left, right)
        private Node SetNode(int index, Node value = null)
        {
            switch (index)
            {
                case 0:
                    return ReturnNode(ref upper, value);
                case 1:
                    return ReturnNode(ref right, value);
                case 2:
                    return ReturnNode(ref lower, value);
                case 3:
                    return ReturnNode(ref left, value);
            }

            return null;
        }

        // returns the node type
        private Node ReturnNode(ref Node type, Node value = null)
        {
            if (value == null) return type;
            else
            {
                type = value;
                return null;
            }
        }
    }
}
