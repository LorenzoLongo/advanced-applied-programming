﻿namespace Globals.Interfaces
{
    // Interface as contract for game physic calculations methods
    public interface IGamePhysics
    {
        void DetectCollision();
    }
}
