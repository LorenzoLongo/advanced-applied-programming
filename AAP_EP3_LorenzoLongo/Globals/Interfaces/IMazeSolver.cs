﻿using Globals.Maze_Generator;
using System.Collections.Generic;
using System.Drawing;

namespace Globals.Interfaces
{
    // Interface with contract for solving mazes algorithms
    public interface IMazeSolver
    {
        List<Node> SolveMaze(int startX, int startY, int endX, int endY, int _width, int _heigth);

        Bitmap DrawShortestPath(int startX, int startY, int _cellsize);
    }
}
