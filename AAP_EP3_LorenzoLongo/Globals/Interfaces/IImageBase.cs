﻿using System.Drawing;

namespace Globals.Interfaces
{
    // Interface as contract for drawing bitmaps
    public interface IImageBase
    {
        int Top { get; set; }
        int Left { get; set; }

        void DrawImage(Graphics g);
        void Update(int x, int y);
        void Dispose();
    }
}
