﻿using Globals.Maze_Generator;
using System.Collections.Generic;
using System.Drawing;

namespace Globals.Interfaces
{
    public delegate void MazeProcessEventHandler(int total, int finished);
    public delegate void MazeCompletedEventHandler();

    // Interface as contract for generating maze algorithms
    public interface IMazeGeneratorBase
    {
        event MazeProcessEventHandler MazeProcess;
        event MazeCompletedEventHandler MazeCompleted;

        List<List<Node>> Nodes{ get; }
        Bitmap MazeBitmap { get; }

        void GenerateMaze(int startX, int startY);

        Bitmap DrawMaze(int startX, int startY, Size cell);
        void CalculateDrawingPoints(int x, int y, int width, int height);

        void OnProcessProgress(int total, int finished);
        void OnCompletedProcess();
    }
}
