﻿namespace Globals.Maze_Generator
{
    // Struct which contains all of the Node class information 
    public struct NodeInfo
    {
        private int index;
        private Node parent;

        public int Index { get { return index; } set { index = value; } }
        public Node Parent { get { return parent; } set { parent = value; } }

        public NodeInfo(int index, Node parent)
        {
            this.index = index;
            this.parent = parent;
        }
    }
}
