﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Threading;

namespace _3D_WPF_EP3
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private const int INTERVAL = 10;
        private bool changed = false;
        private string wallSize;

        private Controls controls = new Controls();
        private Wall wall;
        private MazeBase mazeBase;

        private Transform3DGroup mazeGroup = new Transform3DGroup();
        private Model3DGroup modelGroup = new Model3DGroup();
        private ModelVisual3D model = new ModelVisual3D();

        private GeometryModel3D wallModel;
        private MeshGeometry3D wallMesh;

        private GeometryModel3D baseMazeModel;
        private MeshGeometry3D baseMesh;

        private DiffuseMaterial texture;

        private PointLight light;

        private Transform3DGroup sphereGroup = new Transform3DGroup();
        private TranslateTransform3D sphereTranslate = new TranslateTransform3D();
        private DoubleAnimation animation = new DoubleAnimation();


        // Constructor
        public MainWindow()
        {
            InitializeComponent();

            // Add background inside MainWindow
            ImageBrush texture = new ImageBrush();
            texture.ImageSource = new BitmapImage(new Uri("Resources/BackgroundWall.jpg", UriKind.Relative));
            this.Background = texture;
        }

 
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            // https://stackoverflow.com/questions/11559999/how-do-i-create-a-timer-in-wpf
            DispatcherTimer timer = new DispatcherTimer();
            timer.Tick += new EventHandler(timer_Tick);
            timer.Interval = new TimeSpan(0, 0, 0, 0, INTERVAL);
            timer.Start();

            // Comboboxindex set to 0
            WallSize.SelectedIndex = 0;

            // Add modelGroup to the ModelVisual3D content
            model.Content = modelGroup;

            // Gets and sets the movement of the maze
            mazeGroup = controls.GetRotation();
            model.Transform = mazeGroup;
            Sphere.Transform = mazeGroup;

            viewport.Children.Add(model);

            // Animation of the sphere rolling
            Animation();
        }



        // https://stackoverflow.com/questions/11559999/how-do-i-create-a-timer-in-wpf
        // Gameloop of the maze
        private void timer_Tick(object sender, EventArgs e)
        {
            // Get the selected wall size from the combobox (checks if comboboxvalue is changed) and rebuild the walls
            if (changed == true)
            {
                wallSize = WallSize.SelectedValue.ToString();
                wallSize = wallSize.Substring(38, 1);
                BuildMaze();
                changed = false;
            }
        }

        // Checks if the combobox selected value is changed, sets local boolean to "true"
        private void WallSize_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            changed = true;
        }



        // Method to move the maze, interacts with Control class
        private void MoveMaze(object sender, KeyEventArgs e)
        {
            controls.MoveMaze(sender, e);
        }


        // Method to create the mesh of the base of the maze
        private GeometryModel3D SetBase()
        {
            baseMazeModel= new GeometryModel3D();
            mazeBase = new MazeBase();

            baseMesh = mazeBase.Base();
            baseMazeModel.Geometry = baseMesh;

            SetBaseTexture(baseMazeModel);

            return baseMazeModel;
        }

        // Method which implements the selected texture onto the base of the maze
        private void SetBaseTexture(GeometryModel3D baseMazeModel)
        {
            ImageBrush textureBrush = new ImageBrush();
            textureBrush.ImageSource = new BitmapImage(new Uri("Resources/BackgroundWall.jpg", UriKind.Relative));

            // Create material and add to base model
            texture = new DiffuseMaterial(textureBrush);
            baseMazeModel.Material = texture;
            baseMazeModel.BackMaterial = texture;
        }


        // Method to create the wall mesh and draw it onto the 3D platform
        private GeometryModel3D SetWall(double x1, double x2, double z1, double z2, double height)
        {
            wallModel = new GeometryModel3D();
            wall = new Wall();

            wallMesh = wall.BuildWall(x1, x2, z1, z2, height);
            wallModel.Geometry = wallMesh;

            SetWallTexture(wallModel);

            return wallModel;
        }

        // Method which implements the selected texture onto the walls
        private void SetWallTexture(GeometryModel3D wallModel)
        {
            ImageBrush textureBrush = new ImageBrush();
            textureBrush.ImageSource = new BitmapImage(new Uri("Resources/Walls.jpg", UriKind.Relative));

            // Create material and add to wall model
            texture = new DiffuseMaterial(textureBrush);
            wallModel.Material = texture;
            wallModel.BackMaterial = texture;
        }



        // Method to add all maze models inside the modelgroup and do important calculations
        private void BuildMaze()
        {
            // Calculate the thickness of the wall by the selected combobox value
            var thickness = Double.Parse(wallSize);
            thickness = thickness / 200;

            // Delete all models inside the modelgroup
            modelGroup.Children.Clear();

            // Add the base of the maze to the modelgroup
            modelGroup.Children.Add(SetBase());

            // (Re)-add the walls to the modelgroup
            AddWallsToModelgroup(thickness);

            // Reset sphere centre simultaneously with the selected thickness of the walls
            Sphere.Center = new Point3D(0.85 - thickness, 0.35, 0.8 - thickness);

            // Add pointlight lightsource
            modelGroup.Children.Add(SetLightSource());
        }

        // Method to add and re-add the walls to the modelgroup
        private void AddWallsToModelgroup(double thickness)
        {
            // Primary walls vertical
            modelGroup.Children.Add(SetWall(-1.0, -0.98 + thickness, -1.0, 1.0, 0.3));
            modelGroup.Children.Add(SetWall(1.0, 0.98 - thickness, 1.0, -1.0, 0.3));

            // Primary walls horizontal
            modelGroup.Children.Add(SetWall(-1, 1, 1, 0.98 - thickness, 0.3));
            modelGroup.Children.Add(SetWall(-1.0, 1.0, -1.0, -0.98 + thickness, 0.3));

            // Horizontal walls
            modelGroup.Children.Add(SetWall(-1.0, -0.5, -0.8, -0.78 + thickness, 0.3));
            modelGroup.Children.Add(SetWall(-0.12, 1, -0.8, -0.78 + thickness, 0.3));
            modelGroup.Children.Add(SetWall(-0.3, -0.7, 0.2, 0.18 - thickness, 0.3));
            modelGroup.Children.Add(SetWall(-0.3, -0.7, 0.2, 0.18 - thickness, 0.3));
            modelGroup.Children.Add(SetWall(1, -0.7, 0.70, 0.68 - thickness, 0.3));
            modelGroup.Children.Add(SetWall(0.7, -0.6, 0.4, 0.38 - thickness, 0.3));
            modelGroup.Children.Add(SetWall(-1.0, -0.7, -0.6, -0.58 + thickness, 0.3));
            modelGroup.Children.Add(SetWall(0.5, -0.3, 0.2, 0.18 - thickness, 0.3));
            modelGroup.Children.Add(SetWall(0.3, 0, 0, -0.02 - thickness, 0.3));
            modelGroup.Children.Add(SetWall(0.8, 0.5, -0.5, -0.48 + thickness, 0.3));

            // Vertical walls
            modelGroup.Children.Add(SetWall(-0.5, -0.48 + thickness, -0.8, 0, 0.3));
            modelGroup.Children.Add(SetWall(0, 0.04 + thickness, 0, -0.8, 0.3));
            modelGroup.Children.Add(SetWall(-0.3, -0.28 + thickness, 0.4, -1, 0.3));
            modelGroup.Children.Add(SetWall(-0.14, -0.12 + thickness, -0.1, -0.8, 0.3));
            modelGroup.Children.Add(SetWall(-0.7, -0.68 + thickness, 0, -0.6, 0.3));
            modelGroup.Children.Add(SetWall(0.3, 0.28 - thickness, 0, -0.6, 0.3));
            modelGroup.Children.Add(SetWall(0.5, 0.48 - thickness, 0.2, -0.5, 0.3));
            modelGroup.Children.Add(SetWall(0.7, 0.68 - thickness, 0.4, -0.3, 0.3));
        }



        // Method to create a pointlight lightsource
        public PointLight SetLightSource()
        {
            light = new PointLight();

            light.Color = Colors.LightSkyBlue;
            light.Position = new Point3D(0, 4, 0);
            light.Range = 4.5;

            return light;
        }



        // Method to set an animation onto the sphere
        private void Animation()
        {
            // Set start -and endvalue + interval
            animation.From = 0.0;
            animation.To = -1.5;
            animation.Duration = new Duration(TimeSpan.FromSeconds(2));

            // Set to repeat animation
            animation.AutoReverse = true;
            animation.RepeatBehavior = RepeatBehavior.Forever;

            // Add animation to TranslateTransform3D and set to WPF sphere
            sphereTranslate.BeginAnimation(TranslateTransform3D.OffsetXProperty, animation);
            sphereGroup.Children.Add(sphereTranslate);
            Sphere.Transform = sphereGroup;
        }
    }
}
