﻿using System;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Media3D;

namespace _3D_WPF_EP3
{
    // Class to create the MeshGeometry of the walls of the maze
    public class Wall
    {

        private MeshGeometry3D wall = new MeshGeometry3D();

        private Point3DCollection corners = new Point3DCollection();
        private Int32Collection triangles = new Int32Collection();
        private PointCollection points = new PointCollection();

        // Method to create the walls from the given parameters
        public MeshGeometry3D BuildWall(double x1, double x2, double z1, double z2, double height)
        {
            SetCorners(x1, x2, z1, z2, height);

            FillTriangleIndices();

            AddTextureCoordinates();

            return wall;
        }

        // Method to set the vertex positions of the Mesh
        private void SetCorners(double x1, double x2, double z1, double z2, double height)
        {
            corners.Add(new Point3D(x2, height, z2));
            corners.Add(new Point3D(x1, height, z2));
            corners.Add(new Point3D(x1, 0, z2));
            corners.Add(new Point3D(x2, 0, z2));
            corners.Add(new Point3D(x2, height, z1));
            corners.Add(new Point3D(x1, height, z1));
            corners.Add(new Point3D(x1, 0, z1));
            corners.Add(new Point3D(x2, 0, z1));

            wall.Positions = corners;
        }

        // Method to create the traingles in function of the vertex positions
        private void FillTriangleIndices()
        {
            Int32[] indices ={
            //front
                0,1,2,
                0,2,3,
             //back
                4,7,6,
                4,6,5,
            //Right
                4,0,3,
                4,3,7,
            //Left
                1,5,6,
                1,6,2,
            //Top
                1,0,4,
                1,4,5,
            //Bottom
                2,6,7,
                2,7,3
              };

            foreach (Int32 index in indices)
            {
                triangles.Add(index);
            }

            wall.TriangleIndices = triangles;
        }

        // Method to add the texture coordinates
        private void AddTextureCoordinates()
        {
            points.Add(new Point(1, 0));
            points.Add(new Point(0, 0));
            points.Add(new Point(0, 1));
            points.Add(new Point(1, 1));

            points.Add(new Point(1, 0));
            points.Add(new Point(0, 0));
            points.Add(new Point(0, 1));
            points.Add(new Point(1, 1));

            points.Add(new Point(0, 1));
            points.Add(new Point(0, 0));
            points.Add(new Point(1, 0));
            points.Add(new Point(1, 1));

            points.Add(new Point(0, 1));
            points.Add(new Point(0, 0));
            points.Add(new Point(1, 0));
            points.Add(new Point(1, 1));

            points.Add(new Point(1, 0));
            points.Add(new Point(0, 0));
            points.Add(new Point(0, 1));
            points.Add(new Point(1, 1));

            points.Add(new Point(1, 0));
            points.Add(new Point(0, 0));
            points.Add(new Point(0, 1));
            points.Add(new Point(1, 1));

            wall.TextureCoordinates = points;
        }
    }
}
