﻿using System.Windows.Input;
using System.Windows.Media.Media3D;

namespace _3D_WPF_EP3
{
    // Class which contains the controls of the maze
    public class Controls
    {
        private RotateTransform3D xRotation = new RotateTransform3D();
        private RotateTransform3D zRotation = new RotateTransform3D();

        private AxisAngleRotation3D xAxis = new AxisAngleRotation3D(new Vector3D(1, 0, 0), 0);
        private AxisAngleRotation3D zAxis = new AxisAngleRotation3D(new Vector3D(0, 0, 1), 0);

        private Transform3DGroup group = new Transform3DGroup();

        // https://msdn.microsoft.com/en-us/library/system.windows.forms.keyeventargs.control(v=vs.110).aspx
        // Method which provides the movement of the maze
        public void MoveMaze(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Z && xAxis.Angle > -10)
            {
                xAxis.Angle -= 1;
                xRotation.Rotation = xAxis;
            }
            if (e.Key == Key.S && xAxis.Angle < 10)
            {
                xAxis.Angle += 1;
                xRotation.Rotation = xAxis;
            }
            if (e.Key == Key.Q && zAxis.Angle < 10)
            {
                zAxis.Angle += 1;
                zRotation.Rotation = zAxis;
            }
            if (e.Key == Key.D && zAxis.Angle > -10)
            {
                zAxis.Angle -= 1;
                zRotation.Rotation = zAxis;
            }
        }

        // Getter method to add to HelixViewport3D
        public Transform3DGroup GetRotation()
        {
            group.Children.Add(xRotation);
            group.Children.Add(zRotation);

            return group;
        }
    }
}
