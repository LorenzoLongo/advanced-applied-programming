﻿using System;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Media3D;

namespace _3D_WPF_EP3
{
    // Class to create the MeshGeometry of the bottom of the maze
    public class MazeBase
    {
        private MeshGeometry3D mazeBase = new MeshGeometry3D();

        private Point3DCollection corners = new Point3DCollection();
        private Int32Collection triangles = new Int32Collection();
        private PointCollection points = new PointCollection();

        public MeshGeometry3D Base()
        {
            SetCorners();

            FillTriangleIndices();

            AddTextureCoordinates();

            return mazeBase;
        }

        // Method to set the vertex positions of the Mesh
        private void SetCorners()
        {
            corners.Add(new Point3D(-1, 0, -1));
            corners.Add(new Point3D(-1, 0, 1));
            corners.Add(new Point3D(1, 0, 1));
            corners.Add(new Point3D(1, 0, -1));

            mazeBase.Positions = corners;
        }


        // Method to create the traingles in function of the vertex positions
        private void FillTriangleIndices()
        {
            Int32[] indices ={
            //front
                0,1,2,
                0,2,3,
            };

            foreach (Int32 index in indices)
            {
                triangles.Add(index);
            }

            mazeBase.TriangleIndices = triangles;
        }


        // Method to add the texture coordinates
        private void AddTextureCoordinates()
        {
            //adding TextureCoordinates
            points.Add(new Point(1, 0));
            points.Add(new Point(0, 0));
            points.Add(new Point(0, 1));
            points.Add(new Point(1, 1));

            points.Add(new Point(1, 0));
            points.Add(new Point(0, 0));
            points.Add(new Point(0, 1));
            points.Add(new Point(1, 1));

            points.Add(new Point(0, 1));
            points.Add(new Point(0, 0));
            points.Add(new Point(1, 0));
            points.Add(new Point(1, 1));

            points.Add(new Point(0, 1));
            points.Add(new Point(0, 0));
            points.Add(new Point(1, 0));
            points.Add(new Point(1, 1));

            points.Add(new Point(1, 0));
            points.Add(new Point(0, 0));
            points.Add(new Point(0, 1));
            points.Add(new Point(1, 1));

            points.Add(new Point(1, 0));
            points.Add(new Point(0, 0));
            points.Add(new Point(0, 1));
            points.Add(new Point(1, 1));

            mazeBase.TextureCoordinates = points;
        }
    }
}
