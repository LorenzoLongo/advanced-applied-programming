﻿using Globals.Interfaces;
using Globals.Maze_Generator;
using System;
using System.Collections.Generic;
using System.Drawing;

namespace LogicLayer.Maze_Generator
{
    // Class that contains all of the methods to generate a maze from the Prim algorithm
    // https://github.com/garyng/Maze
    public class Prim : IMazeGeneratorBase, IDisposable
    {
        private int x1, x2, y1, y2;
        private bool disposed = false;

        private List<List<Node>> nodes = new List<List<Node>>();
        private List<Node> nodeX, frontier;

        private Node nodeY, current;
        private NodeInfo parentInfo;

        private Random random;
        private Bitmap maze;
        private Pen p = new Pen(Color.Black, 2);

        public event MazeProcessEventHandler MazeProcess;
        public event MazeCompletedEventHandler MazeCompleted;


        public List<List<Node>> Nodes { get { return nodes; } }
        public Bitmap MazeBitmap { get; private set; }


        // Constructor 
        public Prim(List<List<Node>> nodes)
        {
            this.nodes = nodes;
        }

        // Overloaded constructor 
        public Prim(int width, int height)
        {
            for (int x = 0; x < width; x++)
            {
                nodeX = new List<Node>();
                for (int y = 0; y < height; y++)
                {
                    nodeY = new Node();
                    nodeY.Coordinates = new System.Drawing.Point(x, y);

                    if (y > 0)
                    {
                        nodeY.Upper = nodeX[y - 1];
                        nodeX[y - 1].Lower = nodeY;
                    }
                    if (x > 0)
                    {
                        nodeY.Left = nodes[x - 1][y];
                        nodes[x - 1][y].Right = nodeY;
                    }

                    nodeX.Add(nodeY);
                }

                nodes.Add(nodeX);
            }
        }


        // Calculates the X and Y coordinates for the DrawMaze method 
        public void CalculateDrawingPoints(int x, int y, int width, int height)
        {
            x1 = height * x;
            y1 = width * y;

            x2 = height * (x + 1);
            y2 = width * (y + 1);
        }

        // Method to draw the wall of the maze, also the frontiers and startpoint are drawn with this method
        public Bitmap DrawMaze(int startX, int startY, Size cell)
        {
            maze = new Bitmap(nodes.Count * cell.Width + 1, nodes[0].Count * cell.Height + 1);

            using (Graphics g = Graphics.FromImage(maze))
            {
                for (int x = 0; x < nodes.Count; x++)
                {
                    for (int y = 0; y < nodes[x].Count; y++)
                    {
                        CalculateDrawingPoints(x, y, cell.Width, cell.Height);

                        if (!nodes[x][y].GetWall(0)) g.DrawLine(p, x1, y1, x2, y1);
                        if (!nodes[x][y].GetWall(3)) g.DrawLine(p, x1, y1, x1, y2);
                        if (!nodes[x][y].GetWall(1)) g.DrawLine(p, x2, y1, x2, y2);
                        if (!nodes[x][y].GetWall(2)) g.DrawLine(p, x1, y2, x2, y2);

                        if (nodes[x][y].Frontier) g.FillRectangle(new SolidBrush(Color.FromArgb(100, Color.Cyan)), x1, y1, cell.Width, cell.Height);
                        if (nodes[x][y].StartPoint) g.FillRectangle(new SolidBrush(Color.FromArgb(100, Color.Green)), x1, y1, cell.Width, cell.Height);
                    }
                }
            }
            MazeBitmap = maze;
            return maze;
        }

        // Generates the maze, sets the frontiers, destroys random walls
        public void GenerateMaze(int startX, int startY)
        {
            SetNodes(startX, startY);
            BuildMazeWalls();
            OnCompletedProcess();
        }

        // Method to set the frontier nodes and add the info 
        private void SetNodes(int startX, int startY)
        {
            frontier = new List<Node>();
            random = new Random();

            current = this.Nodes[startX % this.Nodes.Count][startY % this.Nodes[0].Count];
            current.StartPoint = true;
            for (int i = 0; i < current.Count; i++)
            {
                if (current[i] != null)
                {
                    current[i].Info.Add(new NodeInfo(i, current));
                    frontier.Add(current[i]);
                    current[i].Frontier = true;
                }
            }
        }

        // Method to choose random frontier nodes and destroy the walls
        private void BuildMazeWalls()
        {
            int total = this.Nodes.Count * this.Nodes[0].Count;
            int visited = 1;

            while (frontier.Count > 0)
            {
                current = frontier[(int)(random.NextDouble() * 10) % frontier.Count];
                current.Frontier = false;
                frontier.Remove(current);

                // random NodeInfo selection
                parentInfo = current.Info[(int)(random.NextDouble() * 15) % current.Info.Count];

                // Destroy wall => 0-2 1-3
                parentInfo.Parent.DestroyWall(parentInfo.Index);
                current.DestroyWall((parentInfo.Index + 2) % 4);

                // New Frontier
                for (int i = 0; i < current.Count; i++)
                {
                    // Not destroyed
                    if (current[i] != null && current[i].Info.Count == 0 && current[i].MazeWall == 0)
                    {
                        frontier.Add(current[i]);
                        current[i].Info.Add(new NodeInfo(i, current));
                        current[i].Frontier = true;
                    }
                }
                visited++;
                OnProcessProgress(total, visited);
            }
        }

        // Checks the MazeCompleted event
        public void OnCompletedProcess()
        {
            if (MazeCompleted != null) MazeCompleted();
        }

        // Checks the MazeProcess event
        public void OnProcessProgress(int total, int finished)
        {
            if (MazeProcess != null) MazeProcess(total, finished);
        }


        // Dispose method
        public void Dispose()
        {
        }

        // https://docs.microsoft.com/en-us/dotnet/standard/garbage-collection/implementing-dispose
        public virtual void Dispose(bool disposing)
        {
            if (disposed) return;
            if (disposing)
            {
                nodes = null;
                MazeBitmap.Dispose();
            }
            disposed = true;
        }
    }
}
