﻿using Globals.Maze_Generator;
using System;
using System.Collections.Generic;

namespace LogicLayer.Maze
{
    // Class to generate a maze with the Prim algorithm
    public class Prim : IMazeGeneratorrBase
    {
        private int total, visited;

        private List<Node> frontier;
        private Random r;

        private Node current;
        private NodeInfo parentInfo;


        // Constructor
        public Prim(List<List<Node>> nodes) : base(nodes)
        {
        }

        // Constructor overload
        public Prim(int width, int height) : base(width, height)
        {
        }


        // Method to generate the Prim algorithm maze
        public override void GenerateMaze(int startX, int startY)
        {
            total = this.Nodes.Count * this.Nodes[0].Count;
            visited = 1;

            frontier = new List<Node>();
            r = new Random();

            current = this.Nodes[startX % this.Nodes.Count][startY % this.Nodes[0].Count];
            current.StartPoint = true;
            for (int i = 0; i < current.Count; i++)
            {
                if (current[i] != null)
                {
                    current[i].Info.Add(new NodeInfo(i, current));
                    frontier.Add(current[i]);
                    current[i].Frontier = true;
                }
            }

            while (frontier.Count > 0)
            {
                current = frontier[(int)(r.NextDouble() * 10) % frontier.Count];
                current.Frontier = false;
                frontier.Remove(current);

                // Random Parent
                parentInfo = current.Info[(int)(r.NextDouble() * 15) % current.Info.Count];

                    // Break wall => 0-2 1-3
                parentInfo.Parent.DestroyWall(parentInfo.Index);
                current.DestroyWall((parentInfo.Index + 2) % 4);

                // New Frontier
                for (int i = 0; i < current.Count; i++)
                {
                    // No Frontier, no destroyed walls
                    if (current[i] != null && current[i].Info.Count == 0 && current[i].MazeWall == 0)
                    {
                        frontier.Add(current[i]);
                        current[i].Info.Add(new NodeInfo(i, current));
                        current[i].Frontier = true;
                    }
                }
                visited++;
                OnProcessProgress(total, visited);
            }
            OnCompletedProcess();
        }
    }
}
