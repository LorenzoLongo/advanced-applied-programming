﻿using Globals.Maze_Generator;
using System;
using System.Collections.Generic;
using System.Drawing;

namespace LogicLayer.Maze
{
    public abstract class IMazeGeneratorrBase : IDisposable
    {
        private int x1, x2, y1, y2;

        private List<List<Node>> nodes = new List<List<Node>>();
        private List<Node> nodeX;

        private Node nodeY;

        private Bitmap maze;
        private Pen p = new Pen(Color.Black, 2);

        public delegate void MazeProcessEventHandler(int total, int finished);
        public delegate void MazeCompletedEventHandler();

        public event MazeProcessEventHandler MazeProcess;
        public event MazeCompletedEventHandler MazeCompleted;


        public List<List<Node>> Nodes { get { return nodes; } }
        public Bitmap Maze { get; private set; }

        public IMazeGeneratorrBase(List<List<Node>> nodes)
        {
            this.nodes = nodes;
        }

        public IMazeGeneratorrBase(int width, int height)
        {
            for(int x = 0; x < width; x++)
            {
                nodeX = new List<Node>();
                for(int y = 0; y < height; y++)
                {
                    nodeY = new Node();
                    nodeY.Coordinates = new System.Drawing.Point(x, y);

                    if(y > 0)
                    {
                        nodeY.Upper = nodeX[y - 1];
                        nodeX[y - 1].Lower = nodeY;
                    }
                    if(x > 0)
                    {
                        nodeY.Left = nodes[x - 1][y];
                        nodes[x - 1][y].Right = nodeY;
                    }

                    nodeX.Add(nodeY);
                }

                nodes.Add(nodeX);
            }
        }



        public virtual void GenerateMaze(int startX, int startY)
        {
        }

        protected virtual void OnProcessProgress(int total, int finished)
        {
            if (MazeProcess != null) MazeProcess(total, finished);
        }

        protected virtual void OnCompletedProcess()
        {
            if (MazeCompleted != null) MazeCompleted();
        }



        public Bitmap DrawMaze(int startX, int startY, Size cell)
        {
            maze = new Bitmap(nodes.Count * cell.Width + 1, nodes[0].Count * cell.Height + 1);

            using (Graphics g = Graphics.FromImage(maze))
            {
                for(int x = 0; x < nodes.Count; x++)
                {
                    for(int y = 0; y < nodes[x].Count; y++)
                    {
                        CalculateDrawingPoints(x, y, cell.Width, cell.Height);

                        if (!nodes[x][y].GetWall(0)) g.DrawLine(p, x1, y1, x2, y1);
                        if (!nodes[x][y].GetWall(3)) g.DrawLine(p, x1, y1, x1, y2);
                        if (!nodes[x][y].GetWall(1)) g.DrawLine(p, x2, y1, x2, y2);
                        if (!nodes[x][y].GetWall(2)) g.DrawLine(p, x1, y2, x2, y2);

                        if (nodes[x][y].Frontier) g.FillRectangle(new SolidBrush(Color.FromArgb(100, Color.Cyan)), x1, y1, cell.Width, cell.Height);
                        if (nodes[x][y].StartPoint) g.FillRectangle(new SolidBrush(Color.FromArgb(100, Color.Green)), x1, y1, cell.Width, cell.Height);
                    }
                }
            }
            Maze = maze;
            return maze;
        }

        private void CalculateDrawingPoints(int x, int y, int width, int height)
        {
            x1 = height * x;
            y1 = width * y;

            x2 = height * (x + 1);
            y2 = width * (y + 1);
        }

        public void Dispose()
        {
        }

        public virtual void Dispose(bool disposed)
        {
            if(disposed)
            {
                nodes = null;
            }
        }
    }
}
