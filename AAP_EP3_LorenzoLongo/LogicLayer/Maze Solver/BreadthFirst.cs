﻿using Globals.Interfaces;
using Globals.Maze_Generator;
using LogicLayer.Maze;
using System.Collections.Generic;
using System.Drawing;

namespace LogicLayer.Maze_Solver
{
    // Class which contains all of the methods to solve a maze with the Bread First algorithm
    // http://www.koderdojo.com/blog/breadth-first-search-and-shortest-path-in-csharp-and-net-core
    public class BreadthFirst : IMazeSolver
    {
        private IMazeGeneratorrBase maze;
        private Node node;

        private List<Node> neighbours;
        private Dictionary<Node, Node> previousNode;
        private Queue<Node> queue;


        // Constructor
        public BreadthFirst()
        {
        }

        // Overloaded constructor
        public BreadthFirst(IMazeGeneratorrBase maze)
        {
            this.maze = maze;
        }


        // Method to solve the maze with the breadth first algorithm
        public List<Node> SolveMaze(int startX, int startY, int endX, int endY, int _width, int _heigth)
        {
            SetNeighbours(startX, startY, _width);


            return SetShortersPathNodes(startX, startY, endX, endY);
        }

        // Method to set the neighbours queue
        private void SetNeighbours(int startX, int startY, int _width)
        {
            queue = new Queue<Node>();
            previousNode = new Dictionary<Node, Node>();

            queue.Enqueue(maze.Nodes[startX][startY]);

            while (queue.Count > 0)
            {
                node = queue.Dequeue();

                foreach (Node neighbor in GetNeighbours(maze.Nodes, _width))
                {
                    // If previous doesn't contain neighbor, add to queue and add current cell to dictionary with neighbor as key 
                    if (!previousNode.ContainsKey(neighbor))
                    {
                        queue.Enqueue(neighbor);
                        previousNode[neighbor] = node;
                    }
                }
            }
        }

        // Creates a list with all the nodes which provide the shortest path from start to endpoint
        private List<Node> SetShortersPathNodes(int startX, int startY, int endX, int endY)
        {
            var shortestPath = new List<Node>();
            var endPoint = maze.Nodes[endX][endY];

            while (!endPoint.Equals(maze.Nodes[startX][startY]))
            {
                shortestPath.Add(endPoint);
                endPoint = previousNode[endPoint];
            }
            shortestPath.Add(maze.Nodes[startX][startY]);

            return shortestPath;
        }


        // Method to draw the path from start to endpoint onto the bitmap
        public Bitmap DrawShortestPath(int startX, int startY, int _cellsize)
        {
            using (Graphics g = Graphics.FromImage(maze.Maze))
            {
                g.FillRectangle(new SolidBrush(Color.FromArgb(100, Color.Yellow)), _cellsize * startX, _cellsize * startY, _cellsize, _cellsize);
            }

            return maze.Maze;
        }


        // Method to check if the nodes are top, bottom, left, right neighbours
        private List<Node> GetNeighbours(List<List<Node>> _nodes, int _width)
        {
            neighbours = new List<Node>();

            // Check if coordinates are inside the maze grid, if yes check _nodes coordinates are top, bottom, left, right neighbours
            var top = Index(node.Coordinates.X, node.Coordinates.Y - 1, _width) == -1 ? null : _nodes[node.Coordinates.X][node.Coordinates.Y - 1];
            var bottom = Index(node.Coordinates.X, node.Coordinates.Y + 1, _width) == -1 ? null : _nodes[node.Coordinates.X][node.Coordinates.Y + 1];
            var left = Index(node.Coordinates.X - 1, node.Coordinates.Y, _width) == -1 ? null : _nodes[node.Coordinates.X - 1][node.Coordinates.Y];
            var right = Index(node.Coordinates.X + 1, node.Coordinates.Y, _width) == -1 ? null : _nodes[node.Coordinates.X + 1][node.Coordinates.Y];

            if (top != null && _nodes[node.Coordinates.X][node.Coordinates.Y].GetWall(0)) neighbours.Add(top);
            if (right != null && _nodes[node.Coordinates.X][node.Coordinates.Y].GetWall(1)) neighbours.Add(right);
            if (bottom != null && _nodes[node.Coordinates.X][node.Coordinates.Y].GetWall(2)) neighbours.Add(bottom);
            if (left != null && _nodes[node.Coordinates.X][node.Coordinates.Y].GetWall(3)) neighbours.Add(left);

            return neighbours;
        }

        // Check if X and Y Coordinates are inside maze grid
        private int Index(int x, int y, int _width)
        {
            if (x < 0 || y < 0 || x > 30 - 1 || y > 20 - 1) return -1;

            return 1;
        }
    }
}
