﻿using Globals.Interfaces;
using LogicLayer.Properties;
using System;
using System.Drawing;

namespace LogicLayer.Draw
{
    // Class to draw the EndPoint of the maze
    public class EndPoint : IImageBase, IDisposable
    {
        private int x, y;
        private bool disposed = false;

        private Bitmap bitmap;
        private Rectangle endPoint = new Rectangle();

        public int Top { get { return y; } set { y = value; } }
        public int Left { get { return x; } set { x = value; } }

        // Constructor
        public EndPoint()
        {
            this.bitmap = new Bitmap(Resources.EndPoint);

            endPoint.X = Left;
            endPoint.Y = Top;
            endPoint.Width = 18;
            endPoint.Height = 18;
        }


        // Draw the EndPoint Bitmap
        public void DrawImage(Graphics g)
        {
            g.DrawImage(bitmap, x, y);
        }

        // Update X, Y positions of the bitmap
        public void Update(int x, int y)
        {
            Left = x;
            Top = y;

            endPoint.X = Left + 20;
            endPoint.Y = Top - 1;
        }

        // https://docs.microsoft.com/en-us/dotnet/standard/garbage-collection/implementing-dispose
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
            {
                return;
            }
            if (disposing)
            {
                bitmap.Dispose();
            }
            disposed = true;
        }
    }
}
