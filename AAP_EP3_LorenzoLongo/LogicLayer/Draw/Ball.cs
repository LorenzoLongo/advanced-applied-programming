﻿using Globals.Interfaces;
using LogicLayer.Properties;
using System;
using System.Drawing;

namespace LogicLayer.Draw
{
    // Class to draw the ball of the maze
    public class Ball : IImageBase, IDisposable
    {
        private int x, y;
        private bool disposed = false;

        private Bitmap bitmap;
        private Rectangle ball = new Rectangle();

        public int Top { get { return y; } set { y = value; } }
        public int Left { get { return x; } set { x = value; } }

        // Constructor
        public Ball()
        {
            this.bitmap = new Bitmap(Resources.Ball);

            ball.X = Left;
            ball.Y = Top;
            ball.Width = 18;
            ball.Height = 18;
        }


        // Draw the Ball Bitmap
        public void DrawImage(Graphics g)
        {
            g.DrawImage(bitmap, x, y);
        }

        // Update X, Y positions of the bitmap
        public void Update(int x, int y)
        {
            Left = x;
            Top = y;

            ball.X = Left + 25;
            ball.Y = Top + 1;
        }

        // https://docs.microsoft.com/en-us/dotnet/standard/garbage-collection/implementing-dispose
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
            {
                return;
            }
            if (disposing)
            {
                bitmap.Dispose();
            }
            disposed = true;
        }
    }
}
